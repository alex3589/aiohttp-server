from typing import AsyncIterable, Dict
from aiohttp.web_exceptions import HTTPNotFound
from bson import ObjectId
from .models import Item


async def find_all_items() -> AsyncIterable[Item]:
    return Item.find({})


async def find_items(query_dict) -> AsyncIterable[Item]:
    return Item.find(query_dict)


async def create_item(data: Dict) -> Item:
    item = Item(**data)
    await item.commit()
    return item


async def find_item(item_id: ObjectId) -> Item:
    item = await Item.find_one({'_id': item_id})
    if not item:
        raise HTTPNotFound()

    return item
