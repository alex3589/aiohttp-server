from aiohttp import web
from datetime import datetime
from umongo import Document, fields
from .db import instance


@instance.register
class Item(Document):
    name = fields.StringField(required=True)
    description = fields.StrField()
    parameters = fields.DictField()
    created_time = fields.DateTimeField(default=datetime.utcnow)

    class Meta:
        indexes = ['-created_time']


async def ensure_indexes(app: web.Application) -> None:
    await Item.ensure_indexes()
