# Aiohttp REST server

Aiohttp online store server for Python 3.7.4 with mongodb in docker container.

## Requirements:

-  Pyenv, python 3.7.4: https://khashtamov.com/ru/pyenv-python/

- docker: https://www.digitalocean.com/community/tutorials/docker-ubuntu-18-04-1-ru

- docker-compose: https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04-ru


## Usage:

Makefile commands:

1. **venv** — create python env
2. **install** — install requirements
3. **run_server** — run server with  mongo in docker
4. **mongo_stop** — stop mongodb

Example: make venv

## Requests with curl:

### Create item

```shell
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"name":"bike","description":"Single speed", "parameters":{"color":"white","producer":"US"}}' \
  http://localhost:8080/items
```

answer:

```json
{"id": "5e80f081a93b7c812c0d9c36", "description": "Single speed", "created_time": "2020-03-29T19:01:21.220000+00:00", "parameters": {"color": "white", "producer": "US"}, "name": "bike"}
```

### Find item by id

```shell
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"id":"5e80f081a93b7c812c0d9c36"}' \
  http://localhost:8080/items/find-by-id
```

answer:

```json
{"id": "5e80f081a93b7c812c0d9c36", "description": "Single speed", "created_time": "2020-03-29T19:01:21.220000+00:00", "parameters": {"color": "white", "producer": "US"}, "name": "bike"}
```

### Filter item 

Answer is id of items

#### By name

```shell
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"name":"bike"}' \
  http://localhost:8080/items/filter
```

#### By parameter key/value

```shell
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"color":"white"}' \
  http://localhost:8080/items/filter
```

#### filtering both by name and key/value

```shell
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"name":"bike","color":"white"}' \
  http://localhost:8080/items/filter
```

